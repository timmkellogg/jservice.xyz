# jservice.xyz

Hi! Thanks for the interest in the code for
[jservice.xyz](https://jservice.xyz).

## Want to use this?

Are you interested in using this code as an API for instruction? If so, check
out that the [LICENSE](./LICENSE) is MIT, so just use it!

Want to contribute? That's great! There's instructions below about how to
set up your development environment. If you're not interested in that, but
want to send me money, I mean, wow! thank you, that's so nice. Why don't you...

<a href="https://www.buymeacoffee.com/curtissimo" target="_blank">
  <img
    src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png"
    alt="Buy Me A Coffee"
    style="height: 60px !important;width: 217px !important;" >
</a>

## Setting up your development environment

Let's get you set up to get some development done!

Wanna hack on jservice? Here's what you'll need.

### A text editor

You're going to need a text editor, so get the one you like. I happen to use
Neovim, right now, but that's only after a lifetime spent using Emacs. Don't
laugh at Emacs! Watch the Invasion TV show and see that the brilliant software
developer uses Emacs.

### PostgreSQL

This service uses PostgreSQL. You'll want to install that based on your
platform. I use PureOS (a Debian variant) and macOS during my daily life, so
here are the instructions for those. If you use Windows, I'm sorry, not because
I hate Windows, I just remember the PostgreSQL setup being not as much fun as
the \*nix environments. Maybe you can use WSL2.

* **Debian-like**: `sudo apt install postgresql postgresql-contrib`
* **macOS**: `brew install postgresql`

Make sure to complete the installation with whatever instructions it tells you
to do.

Once you have that installed, you'll want to create a user and a database for
this application. Here are some commands for you to create those things. You
may need to add your user name or whatever depending on how things were
installed.

```sh
psql postgres -c "CREATE ROLE jservice WITH LOGIN PASSWORD 'jservice';"
psql postgres -c "CREATE DATABASE jservice WITH OWNER jservice;"
```

Great! Now, you have a user named `jservice` with password `jservice` all set
up with a database named `jservice`. 

You likely want to have some data in there, too. That's what the
[seed](./seed/) directory is for! It's got lots of data in there. Here's how
to get that data into the database:

```sh
cd seed
./create-schema.bash jservice localhost jservice
./import-data.bash jservice
```

Let's make sure that worked. Try typing this to count the number of clues in
the `clues` table:

```sh
psql jservice -c "SELECT COUNT(*) FROM clues;"
```

OMG. Look at all that data!

## Running the Node.js service

This service is built with Node.js v14. Yeah, that's old. I should probably get
around to updating it. You can check out that version in the [.nvmrc](./.nvmrc)
file.

Make sure you have that version of Node.js installed. I use the [Node Version
Manager](https://github.com/nvm-sh/nvm). If you're not on Windows, you can use
it, too! If you are on Windows, well, there's some Windows version of it, but
just use WSL2, eh?

Once you have that installed, use the "correct" version of Node.js, install
the dependencies and start the server.

```sh
nvm use
npm install
npm run dev
```

Are you a Yarn user? Well, do what you do.

Now that it's running you should be able to see it at
<http://localhost:8182/>. Please note that this is written with
JavaScript-standard modules, not CommonJS modules. So, write them correctly
with `export` and `import` and all of the good things that are about good
modules. (Yes, that's an opinion that I don't like CommonJS modules.)

